FROM openjdk:12
ADD target/docker-spring-boot-brunopeixoto.jar docker-spring-boot-brunopeixoto.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-brunopeixoto.jar"]
